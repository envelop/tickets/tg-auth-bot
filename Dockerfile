FROM node:18

COPY ./prisma/ /app/prisma/
COPY ./dist/ /app/dist/
COPY ./src/locales/ /app/src/locales/
#COPY ./src/locales/ /src/locales/
COPY ./node_modules /app/node_modules/
COPY *.json /app/
WORKDIR /app
