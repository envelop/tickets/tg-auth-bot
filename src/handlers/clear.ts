import {prisma} from '../db/index.js'
import {BotContext} from '../utils/types.js'

const clear = async (ctx: BotContext) => {
    if (!ctx.message || !ctx.message.text || ctx.message.chat.type === 'private') {
        return
    }

    const user = await ctx.api.getChatMember(ctx.message.chat.id, ctx.message.from.id)
    if (user.status !== 'creator' && user.status !== 'administrator' && !(user.status === 'left' && user.user.is_bot && user.user.username === 'GroupAnonymousBot')) {
        return
    }

    const msg = ctx.message.text.trim().replace(/\s+/g, ' ').split(' ')[1].toLowerCase()
    let clearSetting: boolean = false
    switch (msg) {
        case 'on':
            clearSetting = true
            break
        case 'off':
            break
        default:
            return ctx.reply(
                ctx.t('wrong-command'),
                {parse_mode: 'Markdown'},
            )
    }
    const group = await prisma.telegramGroup.findFirst({where: {tgGroupId: ctx.message.chat.id}})
    if (!group) {
        return ctx.reply(
            ctx.t('group-not-found'),
            {parse_mode: 'Markdown'},
        )
    }

    if (group.clearSetting !== clearSetting) {
        await prisma.telegramGroup.update({
            where: {id: group.id},
            data: {clearSetting},
        })
    }
    return ctx.reply(
        ctx.t('clear-setting-changed', {text: clearSetting ? 'ON' : 'OFF'}),
        {parse_mode: 'Markdown'},
    )
}

export default clear
