import {Keyboard} from 'grammy'
import {mainMenu} from '../utils/keyboards.js'
import {BotContext} from '../utils/types'
import {prisma} from '../db/index.js'

const info = async (ctx: BotContext) => {
    if (!ctx.message) {
        return
    }

    if (ctx.message.chat.type === 'private') {
        return ctx.reply(
            ctx.t('your-id'),
            {parse_mode: 'Markdown', reply_markup: Keyboard.from(mainMenu).resized()},
        )
    } else {
        const group = await prisma.telegramGroup.findFirst({where: {tgGroupId: ctx.message.chat.id}})
        return ctx.reply(
            ctx.t('chat-info', {text: group?.clearSetting ? 'ON' : 'OFF'}),
            {parse_mode: 'Markdown'},
        )
    }
}

export default info
