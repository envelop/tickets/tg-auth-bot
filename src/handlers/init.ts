import {prisma} from '../db/index.js'
import {BotContext} from '../utils/types.js'

const init = async (ctx: BotContext) => {
    if (!ctx.message || ctx.message.chat.type === 'private') {
        return
    }

    const user = await ctx.api.getChatMember(ctx.message.chat.id, ctx.message.from.id)
    if (user.status !== 'creator' && user.status !== 'administrator' &&
        !(user.status === 'left' && user.user.is_bot && user.user.username === 'GroupAnonymousBot')
    ) {
        return
    }

    const invite = await prisma.telegramGroupInvite.findFirst({
        include: {telegram_group: true},
        where: {telegram_group: {tgGroupId: ctx.message.chat.id}}
    })
    if (invite) {
        return ctx.reply(
            ctx.t('invite-exist', {link: invite.invite}),
            {parse_mode: 'Markdown'},
        )
    }

    const link = await ctx.api.createChatInviteLink(ctx.message.chat.id, {
        name: 'Link with join request',
        creates_join_request: true
    })
    await prisma.telegramGroupInvite.create({
        data: {
            invite: link.invite_link,
            telegram_group: {
                connectOrCreate: {
                    where: {tgGroupId: ctx.message.chat.id},
                    create: {tgGroupId: ctx.message.chat.id, clearSetting: false},
                }
            }
        }
    })
    return ctx.reply(
        ctx.t('invite-created', {link: link.invite_link}),
        {parse_mode: 'Markdown'},
    )
}

export default init
