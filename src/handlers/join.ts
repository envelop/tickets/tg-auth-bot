import {prisma} from '../db/index.js'
import {GETPASS_APP_URL} from '../utils/constants.js'
import {BotContext} from '../utils/types'

const join = async (ctx: BotContext) => {
    const update = ctx.update
    if (!update || !update.chat_join_request) {
        return
    }

    const user = update.chat_join_request.from
    const chat = update.chat_join_request.chat
    const group = await prisma.telegramGroup.findUnique({
        where: {tgGroupId: chat.id},
        include: {
            event: {include: {organizer: true}},
            invite: true,   //todo check invite link
        },
    })
    if (!group) {
        await ctx.declineChatJoinRequest(user.id)
        return ctx.api.sendMessage(
            user.id,
            ctx.t('group-not-found'),
            {parse_mode: 'Markdown'},
        )
    }

    if (group.invite.length === 0) {
        await ctx.declineChatJoinRequest(user.id)
        return ctx.api.sendMessage(
            user.id,
            ctx.t('invite-not-found'),
            {parse_mode: 'Markdown'},
        )
    }

    if (group.event.length === 0) {
        await ctx.declineChatJoinRequest(user.id)
        return ctx.api.sendMessage(
            user.id,
            ctx.t('event-not-connected'),
            {parse_mode: 'Markdown'},
        )
    }

    const profile = await prisma.profile.findMany({
        where: {telegram_id: user.id},
        include: {address: true},
    })
    if (profile.length === 0) {
        await ctx.declineChatJoinRequest(user.id)
        return ctx.api.sendMessage(
            user.id,
            ctx.t('profile-empty', {id: user.id.toString(), profileLink: `${GETPASS_APP_URL}/profile?tgId=${user.id}`, inviteLink: group.invite[0].invite}),
            {parse_mode: 'Markdown'},
        )
    }

    let eventIds: number[] = []
    for (let item of group.event) {
        eventIds.push(item.id)
    }
    let addressIds: number[] = []
    for (let item of profile) {
        addressIds.push(item.address_id)
    }
    const tickets = await prisma.whitelist.findMany({
        where: {event_id: {in: eventIds}, address_id: {in: addressIds}},
        include: {address: true},
    })
    if (tickets.length === 0) {
        await ctx.declineChatJoinRequest(user.id)
        let eventsList: string[] = []
        for (let item of group.event) {
            eventsList.push(`[${item.title}](${GETPASS_APP_URL}/event/${item.organizer.url}/${item.url})`)
        }
        return ctx.api.sendMessage(
            user.id,
            ctx.t('you-have-not-ticket', {eventsList: eventsList.join('\n'), link: group.invite[0].invite}),
            {parse_mode: 'Markdown'},
        )
    }

    await ctx.approveChatJoinRequest(user.id)
    return ctx.api.sendMessage(
        user.id,
        ctx.t('added-to-group', {name: user.first_name || user.username || user.id, link: group.invite[0].invite}),
        {parse_mode: 'Markdown'},
    )
}

export default join
