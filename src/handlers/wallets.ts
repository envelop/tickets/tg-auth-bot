import {Keyboard} from 'grammy'
import {prisma} from '../db/index.js'
import {mainMenu} from '../utils/keyboards.js'
import {BotContext} from '../utils/types.js'

const wallets = async (ctx: BotContext) => {
    if (!ctx.message || ctx.message.chat.type !== 'private') {
        return
    }

    const wallets = await prisma.profile.findMany({
        where: {telegram_id: ctx.message.chat.id},
        include: {address: true},
    })
    return ctx.reply(
        ctx.t('wallets', {walletsList: wallets.map(item => item.address.address).join('\n')}),
        {parse_mode: 'Markdown', reply_markup: Keyboard.from(mainMenu).resized()}
    )
}

export default wallets
