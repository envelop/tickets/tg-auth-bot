import {GETPASS_APP_URL} from '../utils/constants.js'
import {prisma} from '../db/index.js'
import {BotContext} from '../utils/types.js'

const addEvent = async (ctx: BotContext) => {
    if (!ctx.message || !ctx.message.text || ctx.message.chat.type === 'private') {
        return
    }

    const user = await ctx.api.getChatMember(ctx.message.chat.id, ctx.message.from.id)
    if (user.status !== 'creator' && user.status !== 'administrator' && !(user.status === 'left' && user.user.is_bot && user.user.username === 'GroupAnonymousBot')) {
        return
    }

    const eventId = Number(ctx.message.text.trim().replace(/\s+/g, ' ').split(' ')[1])
    if (isNaN(eventId)) {
        return ctx.reply(
            ctx.t('add-event-wrong-command', {link: `${GETPASS_APP_URL}/calendar`}),
            {parse_mode: 'Markdown'},
        )
    }

    const event = await prisma.event.findUnique({where: {id: eventId}})
    if (!event) {
        return ctx.reply(
            ctx.t('event-not-found', {link: `${GETPASS_APP_URL}/calendar`}),
            {parse_mode: 'Markdown'},
        )
    }

    const group = await prisma.telegramGroup.findFirst({
        where: {tgGroupId: ctx.message.chat.id},
        include: {event: {where: {id: eventId}}}
    })
    if (!group) {
        return ctx.reply(
            ctx.t('group-not-found'),
            {parse_mode: 'Markdown'},
        )
    }

    if (group.event.length > 0) {
        return ctx.reply(
            ctx.t('event-already-added'),
            {parse_mode: 'Markdown'},
        )
    } else {
        await prisma.telegramGroup.update({
            where: {id: group.id},
            data: {event: {connect: {id: eventId}}},
        })
        return ctx.reply(
            ctx.t('event-added'),
            {parse_mode: 'Markdown'},
        )
    }
}

export default addEvent
