import {mainMenu} from '../utils/keyboards.js'
import {Keyboard} from 'grammy'
import {BotContext} from '../utils/types'
import {prisma} from '../db/index.js'

const main = async (ctx: BotContext) => {
    if (!ctx.message) {
        return
    }
    if (ctx.message.chat.type === 'private') {
        try {
            if (typeof ctx.match === "string") {
                const hash = await prisma.telegramConnectHash.findUnique({
                    where: {hash: ctx.match},
                    include: {address: true, telegram_group_invite: true},
                })
                if (hash) {
                    const profile = await prisma.profile.findUnique({where: {address_id: hash.address_id}})
                    if (!profile) {
                        await prisma.profile.create({
                            data: {telegram_id: ctx.message.chat.id, address: {connect: {id: hash.address_id}}}
                        })
                    } else if (profile.telegram_id === BigInt(0)) {
                        await prisma.profile.update({
                            where: {id: profile.id},
                            data: {telegram_id: ctx.message.chat.id}
                        })
                    }
                    return ctx.reply(
                        ctx.t('greeting-invite', {link: hash.telegram_group_invite.invite}),
                        {parse_mode: 'Markdown', reply_markup: Keyboard.from(mainMenu).resized()},
                    )
                }
            }
        } catch (e) {
            console.log(e)
        }
        return ctx.reply(
            ctx.t('greeting'),
            {parse_mode: 'Markdown', reply_markup: Keyboard.from(mainMenu).resized()},
        )
    } else {
        if (ctx.message.new_chat_members || ctx.message.left_chat_member) {
            const group = await prisma.telegramGroup.findUnique({where: {tgGroupId: ctx.message.chat.id}})
            if (group && group.clearSetting) {
                await ctx.deleteMessage()
            }
        }
    }
}

export default main
