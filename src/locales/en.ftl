add-event-wrong-command = Wrong command. Use `/addevent 123` where 123 - event ID. You can find event ID on this [page]({$link})

added-to-group = Yoo-hoo! Welcome {$name}!
    You was added to [group]({$link}).

chat-info = Group ID: `{$chatId}`
    Deleting system messages: {$text}

clear-setting-changed = Deleting system messages: {$text}

event-added = Event added.

event-already-added = Event already added.

event-not-connected = The organizer didn't connect any event to the group. So we can't add you to the group yet.

event-not-found = Event not found. You can find event ID on this [page]({$link})

greeting = Hello! It's MyShCh.io bot. Your ID: `{$fromId}`

greeting-invite = Hello! It's MyShCh.io bot. Your [invite link]({$link}) to event

group-not-found = Group not found

invite-created = [Invite link]({$link}) created

invite-exist = [Invite link]({$link}) already exist

invite-not-found = Invite link not found

profile-empty = Oops! We can't find it.  But it's an easy fix.
    1. Please add your telegram ID: `{$id}` on this [page]({$profileLink})
    2. Send new [request]({$inviteLink})

wallets = Your wallets:
    {$walletsList}

    For connect new wallet please go to your [profile]({$appUrl}/profile?tgId={$fromId}) and save your telegram ID: `{$fromId}`

wrong-command = Wrong command

you-have-not-ticket = Oh, we can't add you in group, because you haven't a ticket or you didn't use it yet.
    A list of the events available to you:
    {$eventsList}

    If you have a ticket, then let's use it:
    1. Please use ticket
    2. Send new [request]({$link})

your-id = Your ID: `{$fromId}`
