import {Bot, session} from 'grammy'
import {I18n} from '@grammyjs/i18n'
import * as handlers from './handlers/index.js'
import {BOT_LANG, GETPASS_APP_URL, TG_TOKEN} from './utils/constants.js'
import {BotContext, SessionData} from './utils/types'

const i18n = new I18n<BotContext>({
    defaultLocale: 'en',
    useSession: true,
    directory: './src/locales',
    globalTranslationContext(ctx) {
        return {
            appUrl: GETPASS_APP_URL,
            br: '\n',
            chatId: ctx.message?.chat.id.toString() ?? '',
            firstName: ctx.from?.first_name ?? '',
            fromId: ctx.message?.from.id.toString() ?? '',
        }
    },
    localeNegotiator: (ctx) => (BOT_LANG),
    fluentBundleOptions: {
        useIsolating: false,
    },
})
const initialData: SessionData = {
}

const bot = new Bot<BotContext>(TG_TOKEN)

bot.use(session({
    initial: () => (initialData),
}))
bot.use(i18n)

bot.command('start', handlers.main)

bot.command('addevent', handlers.addEvent)
bot.command('clear', handlers.clear)
bot.command('info', handlers.info)
bot.command('init', handlers.init)

bot.hears('Info', handlers.info)
bot.hears('Wallets', handlers.wallets)

bot.on('message', handlers.main)
bot.on('chat_join_request', handlers.join)

bot.start()
console.log('Bot started')
