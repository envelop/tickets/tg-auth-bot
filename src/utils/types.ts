import {Context, SessionFlavor} from 'grammy'
import {I18nFlavor} from '@grammyjs/i18n'

export interface SessionData {
}

export type BotContext = & Context & I18nFlavor & SessionFlavor<SessionData>
