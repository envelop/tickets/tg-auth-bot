(BigInt.prototype as any).toJSON = function() {
    return this.toString()
}

export const SCENE_NAMES = {
    wallets: 'wallets',
}

export const BOT_LANG = process.env.BOT_LANG?.trim() || 'en'

if (!process.env.GETPASS_APP_URL || !process.env.TG_TOKEN) {
    console.log('.env variables not found')
    process.exit(1)
}
export const GETPASS_APP_URL = process.env.GETPASS_APP_URL.trim()
export const TG_TOKEN = process.env.TG_TOKEN.trim()
