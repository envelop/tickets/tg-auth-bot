# Telegram auth bot for  getpass.is

## Start script

Node 18.12.1 and npm 8.3.0 are used to run. You must set variables in .env file before starting.

### `npm start`

### Development Enviromment
Build
```bash
docker run -it --rm  -v $PWD:/app node:18 /bin/bash -c 'cd /app && npm install && npm run build'
```

Run
```bash
docker build -t tgbot-local .
```

Run
```bash
docker compose -f docker-compose-local2.yaml up
```
_